#!/bin/bash

set -e

docker build -t griffinplus/buildenv/base              base/.
docker build -t griffinplus/buildenv/linux-musl-i686   linux-musl-i686/.
docker build -t griffinplus/buildenv/linux-musl-x86_64 linux-musl-x86_64/.
docker build -t griffinplus/buildenv/mingw-w64-i686    mingw-w64-i686/.
docker build -t griffinplus/buildenv/mingw-w64-x86_64  mingw-w64-x86_64/.
