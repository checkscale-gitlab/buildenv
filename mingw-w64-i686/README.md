# Cross-Build Environment for Windows on Linux using MinGW-w64 (Target: i686)

This image derives from the [Griffin+ Base Build Environment](https://gitlab.com/griffinplus/buildenv/tree/master/base) and
adds everything that is needed to build C/C++ projects for Windows under Linux using [mingw-w64](http://www.mingw-w64.org/).
