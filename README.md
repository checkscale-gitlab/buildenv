[![pipeline status](https://gitlab.com/griffinplus/buildenv/badges/master/pipeline.svg)](https://gitlab.com/griffinplus/buildenv/commits/master)

# Images for the Griffin+ Build Environment based on Ubuntu 18.04

This project consists of the following docker images providing build environments for Griffin+ projects used by the CI pipeline:

- [Base Image](https://gitlab.com/griffinplus/buildenv/tree/master/base)
- [Linux Build Environment for C/C++ projects targeting Linux with musl libc (target: i686)](https://gitlab.com/griffinplus/buildenv/tree/master/linux-musl-i686)
- [Linux Build Environment for C/C++ projects targeting Linux with musl libc (target: x86_64)](https://gitlab.com/griffinplus/buildenv/tree/master/linux-musl-x86_64)
- [Linux Build Environment for C/C++ projects targeting Windows with MinGW-w64 (target: i686)](https://gitlab.com/griffinplus/buildenv/tree/master/mingw-w64-i686)
- [Linux Build Environment for C/C++ projects targeting Windows with MinGW-w64 (target: x86_64)](https://gitlab.com/griffinplus/buildenv/tree/master/mingw-w64-x86_64)
